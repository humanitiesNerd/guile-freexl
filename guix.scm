;;; guile-freexl --- Accessing Excel files from witin Guile
;;; Copyright © 2017 Adriano Peluso <catonano@gmail.com>
;;;
;;; This file is part of guile-freexl.
;;;
;;; guile-freexl is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-freexl is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix packages)
             (guix build-system gnu)
             (guix download)
             (guix git-download)
             (guix gexp)
             ((guix build utils) #:select (with-directory-excursion))
             (gnu packages)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages gnupg)
             (gnu packages xml)
             (guix licenses))

(define %source-dir (dirname (current-filename)))

(define guile-freexl
  (package
    (name "guile-freexl")
    (version "git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system gnu-build-system)
    (arguments
     '(#:configure-flags
       (list (string-append
	      "--with-freexl-libdir="
	      (assoc-ref %build-inputs "freexl")
	      "/lib"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'bootstrap
           (lambda _ (zero? (system* "sh" "bootstrap.sh")))))))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("autoconf" ,autoconf)
       ("automake" ,automake)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile-2.2)
       ("freexl" ,freexl)
       ("bytestrctures" ,guile-bytestructures)))
    (home-page "https://notabug.org/cwebber/guile-gcrypt")
    (synopsis "Accessing Excel files from Guile using Freexl")
    (description "guile-freexl uses Guile's foreign function interface to wrap
libfreexl to provide a variety of Excel files accessing tooling.")
    (license gpl3+)))

guile-freexl
