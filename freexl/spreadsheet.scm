;;; guile-freexl --- xsl access tooling for guile
;;; Copyright © 2017 Adriano Peluso <catonano@gmail.com>
;;;
;;; This file is part of guile-freexl.
;;;
;;; guile-freexl is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-freexl is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (freexl spreadsheet)
  #:use-module (freexl package-config)
  #:use-module (freexl base)
  #:use-module (freexl defines)
  #:use-module (bytestructures guile base)
  #:use-module ((bytestructures guile numeric) #:prefix bs:)
  #:use-module (bytestructures guile vector)
  #:use-module (bytestructures guile struct)
  #:use-module (bytestructures guile union)
  #:use-module (bytestructures guile cstring-pointer)  

  
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)

  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-1)
  
  #:export (	    
	    freexl-get-info
	    freexl-get-worksheet-name
	    freexl-select-active-worksheet
	    freexl-worksheet-dimensions
	    battering-ram
            ))

;;; Commentary:
;;;
;;; Common code for the Freexl bindings.  
;;;
;;; Code:



(define freexl-get-info
  (let* ((ptr     (freexl-func "freexl_get_info"))
         (proc    (pointer->procedure int ptr (list '* unsigned-short '*))))
    (lambda (handle-ptr what)
      (let* ((outcome-ptr (bytevector->pointer (make-bytevector (sizeof '*))))
	     (result (proc (unwrap-handler handle-ptr) what outcome-ptr)))
	(if (not (= result 0))
	    (throw 'get-info-error 'error-code result)
	    (bytevector-uint-ref (pointer->bytevector
				  outcome-ptr (sizeof unsigned-int)) 0
				 (native-endianness) (sizeof int)) )
	))))



(define freexl-get-worksheet-name
  (let* ((ptr     (freexl-func "freexl_get_worksheet_name"))
         (proc    (pointer->procedure int ptr (list '* unsigned-short '*))))
    (lambda (handle-ptr sheet-index)
      (let* ((outcome-ptr (bytevector->pointer (make-bytevector (sizeof '*))))
	     (result (proc (unwrap-handler handle-ptr) sheet-index outcome-ptr)))
	(if (not (= result 0))
	    (throw 'get-worksheet-name-error 'error-code result)
	    (pointer->string (dereference-pointer outcome-ptr))
	)))))


(define freexl-select-active-worksheet
  (let* ((ptr     (freexl-func "freexl_select_active_worksheet"))
         (proc    (pointer->procedure int ptr (list '* unsigned-short))))
    (lambda (handle-ptr sheet-index)
      (let* (
	     (result (proc (unwrap-handler handle-ptr) sheet-index)))
	(if (not (= result 0))
	    (throw 'get-select-worksheet-error 'error-code result)
	    result
	)))))


(define freexl-worksheet-dimensions
  (let* ((ptr     (freexl-func "freexl_worksheet_dimensions"))
         (proc    (pointer->procedure int ptr (list '* '* '*))))
    (lambda (handle-ptr)
      (let* ((rows-bv (make-bytevector (sizeof unsigned-int)))
	     (columns-bv (make-bytevector (sizeof unsigned-short)))
	     (result (proc (unwrap-handler handle-ptr)
			   (bytevector->pointer rows-bv)
			   (bytevector->pointer columns-bv))))
	(unless (zero? result )
	  (throw 'get-worksheet-dimensions-error 'error-code result))
	(list
	 (bytevector-uint-ref rows-bv 0 (native-endianness)
			      (sizeof unsigned-int))
	 
	 (bytevector-uint-ref columns-bv 0 (native-endianness)
			      (sizeof unsigned-short)))))))

(define freexl-cell-value-struct
  (bs:struct
   `((type ,bs:uint8)
     (value ,(bs:union
	      `((int_value ,bs:int)
		(double_value ,bs:double)
		(text_value ,cstring-pointer)))))))

(define freexl_get_cell_value
  (let* ((ptr     (freexl-func "freexl_get_cell_value"))
         (proc    (pointer->procedure int ptr (list '* unsigned-int unsigned-short '*))))
    (lambda (handle-ptr row column my-cell)
      (let* ((result (proc (unwrap-handler handle-ptr)
			   row
			   column
			   ;;pointer to cell-value
			   (bytevector->pointer (bytestructure-bytevector my-cell))
			   )))
	(unless (zero? result)
	  ;;(throw 'get-worksheet-cell-value-error row column 'error-code result)
	  (raise (condition
		  (&error 
		   (error result)))))
	(case (bytestructure-ref  my-cell 'type)
	  ((102) 
	   (bytestructure-ref  my-cell 'value 'int_value))
	  ((103)
	   (bytestructure-ref  my-cell 'value 'double_value))
	  ((101) '())
	  ((104)
	   (bytestructure-ref  my-cell 'value 'text_value))
	  ((105)
	   (bytestructure-ref  my-cell 'value 'text_value))
	  ((106)
	   (bytestructure-ref  my-cell 'value 'text_value))
	  ((107)
	   (bytestructure-ref  my-cell 'value 'text_value))
	  ((108)
	   (bytestructure-ref  my-cell 'value 'text_value))
	  (else "ciao")))
	)))


;; unfold
(define (sheets-list handler sheets-number)
  (define (test current-sheet)
    (>= current-sheet sheets-number))
  (define (sheet-data current-sheet)
    (append (list current-sheet (freexl-get-worksheet-name handler current-sheet))
	  (freexl-worksheet-dimensions handler)))
  
  (unfold test sheet-data 1+ 0))


(define (row-cells handler row columns)
  (let ((my-cell (bytestructure freexl-cell-value-struct)))
    (unfold (lambda (current-column)
	      (>= current-column columns))
	    (lambda (current-column)
	      (guard
		  (condition
		   (else
		    "out of bound"))
		(freexl_get_cell_value handler row current-column my-cell)))
	    1+ 0)))

;;(3 "Ind. 015_P" 121 25)
;;(0 "Metadati" 121 25)

(define (sheet-cells handler sheet-as-a-list)
  (match sheet-as-a-list
    ((sheet sheet-name rows columns)
     (freexl-select-active-worksheet handler sheet)
     (unfold
      (lambda (current-row)
	(>= current-row rows))
      (lambda (current-row)
	(row-cells handler current-row columns))
      1+ 0))))

(define (spreadsheet-cells handler sheets-number)
  (map (lambda (sheet-as-a-list)
	 (cons (cadr sheet-as-a-list) ;;extracting the sheet name
	       (sheet-cells handler sheet-as-a-list)))
       (sheets-list handler sheets-number)))

(define (battering-ram handler)
  (let* (
	 (sheets-number (freexl-get-info handler FREEXL_BIFF_SHEET_COUNT)))
    (spreadsheet-cells handler sheets-number)))


(define (extracted-data path)
  (let* ((handler (freexl-open path))
	 (data (battering-ram handler)))
    (freexl-close handler)
    data))


;;; freexl.scm ends here
