;;; guile-freexl --- xsl access tooling for guile
;;; Copyright © 2017 Adriano Peluso <catonano@gmail.com>
;;;
;;; This file is part of guile-freexl.
;;;
;;; guile-freexl is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-freexl is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (freexl base)
  #:use-module (freexl package-config)
  ;;#:use-module (freexl defines)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  ;;#:use-module (ice-9 match)
  ;;#:use-module ((bytestructures guile) #:prefix bs:)
  #:use-module (srfi srfi-1)
  ;;#:use-module ((guix licenses) #:prefix license:)
  #:export (freexl-version
            freexl-func
	    freexl-open
	    freexl-close
	    handler?
	    wrap-handler unwrap-handler
            ))



(define-wrapped-pointer-type handler
  handler?
  wrap-handler unwrap-handler
  (lambda (obj port)
    (format port "#<xsl-file of ~a ~a>"
	    ;;I should probably show the file path, here
	    (number->string (object-address obj) 16)
	    (pointer-address (unwrap-handler obj)))))
 
(define freexl-func
  (let ((lib (dynamic-link %freexl)))
    (lambda (func)
      "Return a pointer to symbol FUNC in freexl."
      (dynamic-func func lib))))

(define freexl-version
  (let* ((ptr     (freexl-func "freexl_version"))
         (proc    (pointer->procedure '* ptr '(*)))
         (version (pointer->string (proc %null-pointer))))
    (lambda ()
      "Return the version number of freexl as a string."
      version)))

(define freexl-open
  (let* ((ptr     (freexl-func "freexl_open"))
         (proc    (pointer->procedure int ptr '(* *)))
	 ;;const char *path, const void **xls_handle
	 )
    (lambda (path)
      "maps the freexl-open in the library. 
The only difference is that if it can't open the file, 
it will raise an exception.
It takes a path, returns a handler.
Such handler will have to be passed as an argument to all 
the subsequent calls to other funtions of the library"
      (let* ((handle-ptr (bytevector->pointer (make-bytevector (sizeof '*))))
	     (path-ptr (string->pointer path))
	     (result (proc path-ptr handle-ptr)))
	(if (not (= result 0))
	    (throw 'open-error 'error-code result)
	    (wrap-handler (dereference-pointer handle-ptr)));;<-- this is the most important line
	                                                    ;;    of the whole procedure !!
	))))


;;ret = freexl_close (handle);

(define freexl-close
  (let* ((ptr     (freexl-func "freexl_close"))
         (proc    (pointer->procedure int ptr '(*)))
	 ;;const char *path, const void **xls_handle
	 )
    (lambda (handler-ptr)
      (let* (
	     (result (proc (unwrap-handler handler-ptr))))
	(unless (= result 0)
	  (throw 'open-error 'error-close result))
	))))



