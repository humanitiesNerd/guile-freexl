;;; guile-freexl --- xls files reading tooling for guile
;;; Copyright © 2017 Adriano Peluso <catonano@gmail.com>
;;;
;;; This file is part of guile-gcrypt.
;;;
;;; guile-gcrypt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-freexl is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-freexl.  If not, see <http://www.gnu.org/licenses/>.

(define-module (freexl common-test)
  #:use-module (freexl spreadsheet)
  #:use-module (freexl common)
  #:use-module (freexl defines)
  #:use-module (srfi srfi-64)
;;  #:use-module (rnrs bytevectors)
  )

(test-begin "freexl common-test")


(test-equal "number of sheets in the file"
  28
  (freexl-get-info (freexl-open "resources/Lavoro_P.xls")
		   FREEXL_BIFF_SHEET_COUNT))

(test-equal "name of the sheet at a given index"
  "Metadati"
  (freexl-get-worksheet-name (freexl-open "resources/Lavoro_P.xls")
			     0))

(test-equal "dimensions of the sheet no. 0"
  '(118 17)
  (let ((my-file (freexl-open "resources/Lavoro_P.xls")))
   (freexl-select-active-worksheet my-file 0)
   (freexl-worksheet-dimensions my-file)))



(test-end "freexl common-test")

