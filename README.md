# guile-freexl

This is a wrap of [Freexl](https://www.gaia-gis.it/fossil/freexl/index) made in [Guile Scheme](https://www.gnu.org/software/guile/)

I used the Guile functionality to load libraries made in C and call C functions from Scheme

This is a modest and boring piece of software but I hope it could be useful to some fellow guilers out there, to munge some data, maybe.

Maybe some open data activist.

I'd be glad if you would let me know that you used guile-freexl.

# acknowledgments and thanks givings

I have to mention Taylan for their [Bytestructures](https://github.com/TaylanUB/scheme-bytestructures/) library.

Freexl populates a C struct when reading the contents of a cell so some facility for dealing with structs was needed. The Guile API for that is a bit rough.

So thank you Taylan !

Also thanks the the people on the guile-user mailing list that I pestered with naive questions.

#How to use guile-freexl

Just call extracted-data with a string representing a path to an xsl file.

It will return a nested list representing all the values from all the sheets in the file.

You can further procees such nested list from within your own piece of software, maybe to store data into a db or write them in a different format.

You can filter such structure to extract, for example, a list of sheets names.


# TODO

## improving exceptions
when an attempt to read an out of bound cell is made, the code raises and catches an exception.

The exception related code could check which error code the counterpart C function returned and forward such infornation somehow. 

## returning proper types for datetimes, dates, times
Also, when returning datetimes, times and dates, Freexl returns simple strings. It's necessary that some parsing is made and something of the appropriate type is returned instead.

When writing this, I can't say exactly, but I remember that some GNU library devoted to datetimes does exist. An investigation is necessary.

## dealing with C conventions
Also some solution for dealing with those C things like `#define SOMETHING some_value` should be found

# Shortcomings and woes on my side
So while working on guile-freexl I happened to read the Guile manual for the so called dynamic wind.

I was tinking to offer some `with-file` thing to have `freexl-open` and `freexl-close` automatically called.

freexl-close, particulary, cleans the memory used to represent the data in the file.

But I found the manual too hard to read and I found the discussion that I started on the guile-user mailing list somewhat frustrating.

I still am not sure wether or not dynamic wind is intended for use cases like mine.

So for now there's no such functionality and you'll have to remember to explicitly call `freexl-open` and `freexl-close`.

I'd appreciate any clarification on the issue, the kind of clarification you would give to your 4 years old niece.

